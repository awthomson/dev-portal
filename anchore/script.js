var results = null;

function reset() {
	$('#package_table thead').html('<tr><td>Package</td><td>Version</td><td>License</td></tr>');
	$('#package_table tbody').html('<tr></tr>');

}

function get_info(image) {

	$("#package_table tbody").empty();
	$('#package_table tbody').html('<tr></tr>');
	$("#vuln_table tbody").empty();
	$('#vuln_table tbody').html('<tr></tr>');

	results.images.forEach(function(item, index) {
		if (item.name == image) {
			item.packages.forEach(function(item) {
				$('#package_table tbody tr:last').after('<tr><td>'+item.name+'</td><td>'+item.version+'</td><td>'+item.license+'</td>></tr>');
			});
			item.vulnerabilities.forEach(function(item) {
				$('#vuln_table tbody tr:last').after('<tr class="'+item.sev+'"><td><a href="'+item.link+'">'+item.cve+'</a></td><td>'+item.package+'</td><td>'+item.fix+'</td><td>'+item.sev+'</td>></tr>');
			});
		}
	});

	/*
	$([document.documentElement, document.body]).animate({
        	scrollTop: $("#packages").offset().top
    	}, 2000);
	*/

        $("#packages").scrollTop(100);

 
}

function update_table() {

	$("#image_table tbody").empty();
	$('#image_table tbody').html('<tr></tr>');

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			results = JSON.parse(this.responseText);
			results.images.forEach(function(item, index) {
				var cnt_high = 0;
				var cnt_medium = 0;
				var cnt_low = 0;
				item.vulnerabilities.forEach(function(xx) {
					if ((xx.sev == "High") || (xx.sev == "Critical"))  {
						cnt_high++;
					} else if (xx.sev == "Medium") {
						cnt_medium++;
					} else {
						cnt_low++;
					}
				});
				$('#image_table tbody tr:last').after('<tr onclick="get_info(\''+item.name+'\');"><td>'+item.name+'</td><td>'+item.packages.length+'</td><td>'+cnt_high+'</td><td>'+cnt_medium+'</td><td>'+cnt_low+'</td></tr>');
			});
		}
	};
	xhttp.open("GET", "results.json", true);
	xhttp.send();
}
