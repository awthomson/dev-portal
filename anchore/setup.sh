#!/bin/bash

unlink /var/www/html/index.html
unlink /var/www/html/script.js
unlink /var/www/html/style.css
unlink /var/www/html/results.json
ln -s /home/alex/Projects/dev-portal/anchore/index.html /var/www/html/index.html
ln -s /home/alex/Projects/dev-portal/anchore/script.js /var/www/html/script.js
ln -s /home/alex/Projects/dev-portal/anchore/style.css /var/www/html/style.css
ln -s /home/alex/Projects/dev-portal/anchore/results.json /var/www/html/results.json

