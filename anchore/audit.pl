#!/usr/bin/perl

use JSON;

open(my $fh, '>', 'results.json');
get_containers();
# scan_image(postgres);
close $fh;
exit 0;

sub get_containers {
	# @images = `docker images | grep latest | awk '{print \$1}'`;
	@images = (
		"frontend:0.0.25",
		"customer:1.7",
	 	"graphql:1.0.10",
		"pointofconnection:1.24",
		"grid-asset:1.5",
		"location:1.13",
		"apidataintegration:1.0.11",
        "neo4j:latest"
	);	
	print "@images\n";
	print $fh "{ \"images\": [\n";
	my $i_cnt = 0;
	for $i (@images) {
		chomp($i);
		print $fh "  { \"name\": \"$i\",\n";
		my @packages = get_packages($i);
		my @vulns = get_vulns($i);
		if ($i_cnt == $#images) {
			print $fh "\n";
		} else {
			print $fh ",\n";
		}
		$i_cnt++;
	}
	print $fh "  ]\n}\n";
}

sub get_packages {
	my $image_name = $_[0];
	$image_name="nexus.es.transpower.co.nz:5002/$image_name";
	print "Scanning packages for $image_name...\n";
	print "cd ~/aevolume && docker-compose exec engine-api anchore-cli image add $image_name\n";
	my $x = `cd ~/aevolume && docker-compose exec engine-api anchore-cli image add $image_name`;
	$x = `cd ~/aevolume && docker-compose exec engine-api anchore-cli image wait $image_name`;
	my @installed = `cd ~/aevolume && docker-compose exec engine-api anchore-cli image content $image_name os | grep -v Package`;
	print $fh "    \"packages\": [\n";
	my $x_cnt = 0;
	foreach $x (@installed) {
		chomp($x);
		($package, $ver, $lic) = split(/\s+/, $x);
		print $fh "      { \"name\": \"$package\", \"version\": \"$ver\", \"license\": \"$lic\" }";
		if ($x_cnt == $#installed) {
			print $fh "\n";
	       	} else {
			print $fh ",\n";
		}
		$x_cnt++;
	}
	print $fh "     ],\n";
	my $num_packages = @installed;
	print "# Packages: $num_packages\n";
}

sub get_vulns {
	my $image_name = $_[0];
	$image_name="nexus.es.transpower.co.nz:5002/$image_name";
	print "Scanning vulnerabilities for $image_name...\n";
	my @vulns = `cd ~/aevolume && docker-compose exec engine-api anchore-cli image vuln $image_name all | grep -v Vuln`;
	print $fh "    \"vulnerabilities\": [\n";
	my $x_cnt = 0;
	foreach $x (@vulns) {
		chomp($x);
		($cve, $pack, $sev, $fix, $url) = split(/\s+/, $x);
		print $fh "      { \"cve\": \"$cve\", \"package\": \"$pack\", \"sev\": \"$sev\", \"link\": \"$url\", \"fix\": \"$fix\" }";
		if ($x_cnt == $#vulns) {
			print $fh "\n";
	       	} else {
			print $fh ",\n";
		}
		$x_cnt++;
	}
	print $fh "    ]}";
	print "# Vuls $#vulns\n";
}

